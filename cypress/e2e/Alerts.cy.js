/// <reference types = "cypress" />
/// <reference types="@cypress/xpath"/>
require('@4tw/cypress-drag-drop')

Cypress.on('uncaught:exception', (err, runnable) => { return false; });

describe("Uso de Alertas",()=>{

    it.only('Manejo de Alertas1 - Alerta Simple', () => {
        cy.visit("https://demoqa.com/alerts")
        cy.wait(1000)  
        cy.xpath("//button[@id='alertButton']").click()

        cy.on("window:alert",(str)=>{        
          expect(str).to.equal("You clicked a button")
         })

    });

    it('Manejo de Alertas2 - Confirmacion respuesta OK', () => {
        cy.visit("https://demoqa.com/alerts")
        cy.wait(1000)  
        cy.xpath("//button[@id='confirmButton']").click()

        cy.on("window:confirm",(str)=>{        
          expect(str).to.equal("Do you confirm action?")
         })
         cy.xpath("//span[@id='confirmResult']").should('have.text', 'You selected Ok')
         cy.wait(2000)
    });


    it('Manejo de Alertas3 - Confirmacion respuesta NOK', () => {
        cy.visit("https://demoqa.com/alerts")
        cy.wait(1000)  
        cy.xpath("//button[@id='confirmButton']").click()

        cy.on("window:confirm",(str)=>{        
          expect(str).to.equal("Do you confirm action?")
          return false; 
        })
         cy.xpath("//span[@id='confirmResult']").should('have.text', 'You selected Cancel')
         cy.wait(2000)
    });


    it('Manejo de Alertas4- "Ingresando" Texto', () => {
        cy.visit("https://demoqa.com/alerts")
        cy.wait(1000)
       
        cy.window().then(str=>{        
          cy.stub(str,'prompt').returns('Pauly')
          cy.xpath("//button[@id='promtButton']").should("be.visible").click()
        })
        cy.xpath("//span[@id='promptResult']").should('have.text', 'You entered Pauly')
        cy.wait(2000)
    });

})
    
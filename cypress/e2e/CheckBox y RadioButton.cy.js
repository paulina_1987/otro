/// <reference types = "cypress" />
/// <reference types="@cypress/xpath"/>
Cypress.on('uncaught:exception', (err, runnable) => { return false; });

describe("CheckBox y RabioButton", () => {

    it('CheckBox', () => {
        cy.visit("https://demoqa.com/checkbox")
        cy.wait(1000)
        cy.xpath('//button[@class="rct-collapse rct-collapse-btn"]').click({ force: true }) //despliega listado
        cy.wait(2000)

        cy.get(':nth-child(2) > .rct-text > label > .rct-checkbox').click()
        cy.wait(1000)
       cy.get(':nth-child(3) > .rct-text > label > .rct-checkbox').click()
        cy.wait(1000)

    });
 


    it('RadioButton', () => {
        cy.visit("https://demoqa.com/radio-button")
        cy.wait(2000)

        cy.xpath("//input[@id='yesRadio']").click({force: true})
        cy.wait(2000)

        cy.xpath("//p/span/text()").then((str)=>{
          cy.log("\nLa primera vez se marcó la opción "+str.text())
        })


        cy.xpath("//input[@id='impressiveRadio']").click({force: true})
        cy.wait(2000)

        cy.xpath("//p/span/text()").then((str2)=>{
            cy.log("La segunda vez se marcó la opción "+str2.text())
          })
    });
})

///<reference types = "cypress"/>

describe("Elementos2", () => {
    it('Validar descarga de archivo', () => {

        cy.visit("https://demoqa.com/upload-download")
        cy.wait(1000)

        cy.get('#downloadButton').scrollIntoView().click()
        cy.wait(1000)

        cy.verifyDownload('sampleFile.jpeg')
        cy.wait(1000)
    });


    it('Validar apertura de pestaña', () => {       //pestaña o ventana se manejan igual

        cy.visit("https://demoqa.com/browser-windows")
        cy.wait(1000)

        cy.window().then((win) => {
            cy.spy(win, 'open').as('windowOpen'); // 'spy' vs 'stub' lets the new tab still open if you are visually watching it
        });

        cy.get('#tabButton').scrollIntoView().click()
        cy.wait(2000)

        cy.get('@windowOpen').should('be.calledWith', '/sample')
        cy.wait(1000)

    });

    it('Validar apertura de ventana', () => {       //pestaña o ventana se manejan igual

        cy.visit("https://demoqa.com/browser-windows")
        cy.wait(1000)

        cy.window().then((win) => {
            cy.spy(win, 'open').as('windowOpen');
        });

        cy.get('#windowButton').scrollIntoView().click()
        cy.wait(2000)

        cy.get('@windowOpen').should('be.calledWith', '/sample')
        cy.wait(1000)

    });

    it.only(' Carga nuevo tab en pestaña actual y se retorna a original', () => {       //pestaña o ventana se manejan igual

        cy.visit("https://demoqa.com/browser-windows")
        cy.wait(1000)

        let newUrl = '';
        cy.window().then((win) => {
          cy.stub(win, 'open').as('popup').callsFake(url => {
            win.location.href = url;
          });
        })

        // Click button which triggers javascript's window.open() call
        cy.get('#tabButton').scrollIntoView().click()

        // Make sure that it triggered window.open function call
        cy.get("@popup").should('be.calledWith', '/sample')
        cy.wait(1000)

        cy.get('#sampleHeading').should('have.text', 'This is a sample page')
        cy.wait(1000)
        cy.go('back')
    });

})

Cypress.on('uncaught:exception', (err, runnable) => {
    return false
});
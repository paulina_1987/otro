/// <reference types = "cypress" />
Cypress.on('uncaught:exception', (err, runnable) => { return false; });
/// <reference types="@cypress/xpath"/>

describe("Uso de ForEach", () => {

    it('Uso de ForEach', () => {
        cy.visit("https://computer-database.gatling.io/computers")
        cy.wait(2000)
        cy.xpath("//tbody//tr//td").each(($el,index,$list)=>{
 
            if(!($el.text().includes("-")) ||($el.text().startsWith("AN/FSQ"))){
            cy.log("\nElemento n°"+index+"->"+$el.text()+"\n") 
            }
            // cy.log("\nList ->"+$list.text()+"\n") //contenido de la lista completa


           if($el.text().includes("IBM")){
            cy.log("\nLa palabra IBM aparece en la fila "+index)     
           }
        })
        
    });

})

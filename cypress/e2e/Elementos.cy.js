///<reference types = "cypress"/>
require('cypress-plugin-tab')

describe("Force-True, Scroll y Assert",()=>{

    it('Usando Force True', () => {
        cy.visit("https://testsheepnz.github.io/BasicCalculator.html") // carga de url
        cy.wait(1000) //espera

        cy.get('[data-testid="calcForm"] > h2').scrollIntoView() // mueve scroll hasta elemento
        cy.wait(1000)
        cy.get('[data-testid="number1Field"]').type('8').tab().type('10') // ingreso datos y uso de tab
        cy.wait(1000)

        cy.get('[data-testid="calculateButton"]').click({force:true}) //click en boton con force:true
        cy.wait(1000)

        cy.get('[data-testid="numberAnswerField"]').then((e)=>{
            let valor = e.val()
            cy.log('La suma es: ' +valor)
            if(valor== 18)
                cy.log("\n OK")
        })
        cy.wait(2000)
    });

    it('Usar Select y Checkbox', () => {
        cy.visit("https://testsheepnz.github.io/BasicCalculator.html") // carga de url
        cy.wait(1000) //espera

        cy.get('[data-testid="calcForm"] > h2').scrollIntoView() // mueve scroll hasta elemento
        cy.wait(1000)
        cy.get('[data-testid="number1Field"]').type('300').tab().type('520').tab() // ingreso datos y uso de tab
        cy.wait(1000)

        cy.get('[data-testid="selectOperationDropdown"]').select('1').should('have.value', '1')// usando Select
        cy.wait(1000)

        cy.get('[data-testid="integerSelect"]').should("not.be.checked").check()

        cy.get('[data-testid="calculateButton"]').click({force:true}) //click en boton con force:true
        cy.wait(1000)

        cy.get('[data-testid="numberAnswerField"]').then((e)=>{
            let valor = e.val()
           if(valor.startsWith("-")){
                cy.log("\n El número no es entero")
            }
        })
        cy.wait(2000)

    });

})
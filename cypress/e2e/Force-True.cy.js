///<reference types = "cypress"/>
require('cypress-plugin-tab')

describe("Force-True",()=>{

    it('Usando Force True', () => {
        cy.visit("https://testingqarvn.com.es/")
        cy.wait(2000)

        cy.get('#top-menu > #menu-item-179 > [href="https://testingqarvn.com.es/practicas-qa/"]').click({force:true})
        cy.wait(1000)
        cy.contains('Datos Personales').click({force:true})
        cy.wait(2000)

    });

})
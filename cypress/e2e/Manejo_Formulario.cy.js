/// <reference types = "cypress" />
require('cypress-plugin-tab')


describe("Ejemplo Formulario",()=>{

    it('Completando Formulario', () => {
        cy.visit("https://demoqa.com/automation-practice-form")
        cy.wait(1000)
        
        cy.get('h5').scrollIntoView()
        cy.wait(1000)

        cy.get('#firstName').type('Paulina').tab().type('Nanjari')
        .tab().type('correo@mail.com')

        cy.wait(1000)
        cy.get('[type="radio"]').check('Female',{force:true})// radiobutton

        cy.wait(1000)
        cy.get('#userNumber').type('0123456789').tab().type('07/01/1987').type('{esc}')

        cy.wait(1000)
        cy.get('#hobbies-checkbox-2').should("not.be.checked").check({force:true})// checkbox2

        cy.wait(1000)
        cy.get('#hobbies-checkbox-3').should("not.be.checked").check({force:true})// checkbox3
        cy.wait(1000)

        cy.get('#uploadPicture').should('be.visible').selectFile('cypress/fixtures/shinobu.jpg')
        cy.wait(1000)

        cy.get('#currentAddress').should('be.visible').type('Mi dirección no te la diré jaja').tab().tab().click()
        cy.wait(1000)
    });
})

Cypress.on('uncaught:exception', (err, runnable) => {
    return false
    });
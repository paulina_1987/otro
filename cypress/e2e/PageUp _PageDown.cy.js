/// <reference types = "cypress" />

describe("Ejemplo2",()=>{

    it('PageUp - PageDown', () => {
        cy.visit("https://testingqarvn.com.es/datos-personales/")
        cy.wait(1000)
        cy.title().should('eq','Datos Personales | TestingQaRvn') // valida título
        cy.wait(1000)
        cy.get('#wsf-1-field-28').type('{pageup}')
        cy.wait(2000)

        cy.get('#wsf-1-field-21').type('{pagedown}')// no se observa diferencia entre uno y otro
        cy.wait(2000)
    });
})
    
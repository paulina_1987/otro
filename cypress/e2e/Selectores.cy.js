/// <reference types = "cypress" />
/// <reference types="@cypress/xpath" />
Cypress.on('uncaught:exception', (err, runnable) => { return false; });

describe("Selectores",()=>{

    it('Selector por Atributo', () => {
        cy.visit("https://devmoqa.com/text-box")
        cy.wait(1000)

        cy.get("[placeholder='Full Name']").should("be.visible").type("Paulina Nanjarí1")
    });

    
    it('Selector por ID', () => {
        cy.visit("https://demoqa.com/text-box")
        cy.wait(1000)

        cy.get("#userName").should("be.visible").type("Paulina Nanjarí2")
    });


    it.only('Selector por Xpath', () => {
        cy.visit("https://demoqa.com/text-box")
        cy.wait(2000)

        cy.xpath('//input[@id="userEmail"]').type("Paulina Nanjarí3")//.should("be.visible").type("Paulina Nanjarí3")
    });


    it('Selector Contains', () => {
        cy.visit("https://testingqarvn.com.es/")
        cy.wait(2000)
        cy.get('#top-menu > #menu-item-179 > [href="https://testingqarvn.com.es/practicas-qa/"]').click({force:true})
        cy.wait(1000)
        cy.contains('Datos Personales').click({force:true})
        cy.wait(2000)

    });
})
    
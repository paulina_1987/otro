///<reference types = "cypress"/>
require('cypress-plugin-tab')

describe("Tab",()=>{

    it('Uso de Tab', () => {
        cy.visit("https://testingqarvn.com.es/datos-personales/")
        cy.wait(1000)
        cy.get("#wsf-1-field-21").type("Paulina Andrea").tab()
        .type("Nanjari López").tab().type("paulina_1987@live.cl")
        .tab().type("0993201474").tab().type("Mi Casa")
        cy.wait(2000)

        cy.get('#wsf-1-field-27').should("be.visible").click()
    });

})
# cypress/e2e/duckduckgo.feature
Feature: Ejemplo1
  Scenario: visiting the frontpage
  Given Accedo a la url del sitio
  When Ingreso el campo "Paulina Andrea" en el nombre
  And Ingreso el campo "Nanjarí López" en el apellido
  And Ingreso el campo "paulina123@correo.cl" en el email
  And Ingreso el campo "+56994335566" en el telefono
  And Ingreso el campo "Jose Maria Caro Block 275 Depto 4" en la direccion
  And Presiono el boton enviar
  Then Visualizo confirmacion de envio formulario "Gracias por tu encuesta."
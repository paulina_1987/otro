# cypress/e2e/duckduckgo.feature
Feature: Ejemplo2 con Scenario Outline
  Scenario Outline: Scenario Outline name: visiting the frontpage
  Given Navego a la url del sitio
  When Tipeo el campo <nombre> en el nombre
  And Tipeo el campo <apellido> en el apellido
  And Tipeo el campo <email> en el email
  And Tipeo el campo <telefono> en el telefono
  And Tipeo el campo <direccion> en la direccion
  And Click en el boton enviar
  Then Valido confirmacion de envio formulario <mensaje>

  Examples:
      |      nombre         |    apellido     |         email             ||    telefono   ||             direccion             ||        mensaje            |
      | "Paulina Andrea"    | "Nanjarí López" | "paulina123@correo.cl"    || "+56994335566"||"Jose Maria Caro Block 275 Depto 4"||"Gracias por tu encuesta." |
      | "Sebastian Nicolas" | "Nanjarí López" | "sebastian123@correo.cl"  || "+56994387656"||"Jose Maria Caro Block 277 Depto 6"||"Gracias por tu encuesta." |
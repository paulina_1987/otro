import { Given,When, Then } from "@badeball/cypress-cucumber-preprocessor";
/// <reference types="@cypress/xpath"/>

Given("Accedo a la url del sitio", () => {
    cy.visit("https://testingqarvn.com.es/datos-personales/");
  });

When("Ingreso el campo {string} en el nombre", (nombre) => {
    cy.xpath("//input[@id='wsf-1-field-21']").type(nombre)
});

When("Ingreso el campo {string} en el apellido", (apellido) => {
    cy.xpath("//input[@id='wsf-1-field-22']").type(apellido)
});

When("Ingreso el campo {string} en el email", (email) => {
    cy.xpath("//input[@id='wsf-1-field-23']").type(email)
});

When("Ingreso el campo {string} en el telefono", (telefono) => {
    cy.xpath("//input[@id='wsf-1-field-24']").type(telefono)
});

When("Ingreso el campo {string} en la direccion", (direccion) => {
    cy.xpath("//textarea[@id='wsf-1-field-28']").type(direccion)
});

When("Presiono el boton enviar", () => {
    cy.xpath("//button[@id='wsf-1-field-27']").click()
});

Then("Visualizo confirmacion de envio formulario {string}", (mensaje) => { 
    cy.wait(2000)
    cy.get('p').then(($rescataTexto)=>{
        let valor = $rescataTexto.text()
        if(expect(valor).equal(mensaje)){
            cy.log("\n Se realizó el envío de datos")
        }    
    })                     
});